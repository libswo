/*
 * This file is part of the libswo project.
 *
 * Copyright (C) 2015 Marc Schink <swo-dev@marcschink.de>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LIBSWOCXX_HPP
#define LIBSWOCXX_HPP

#include <stdint.h>
#include <string>
#include <vector>
#include <stdexcept>
#include <type_traits>

#include <libswo/libswo.h>

namespace libswo
{

enum class LogLevel {
	NONE = LIBSWO_LOG_LEVEL_NONE,
	ERROR = LIBSWO_LOG_LEVEL_ERROR,
	WARNING = LIBSWO_LOG_LEVEL_WARNING,
	INFO = LIBSWO_LOG_LEVEL_INFO,
	DEBUG = LIBSWO_LOG_LEVEL_DEBUG
};

enum class PacketType {
	UNKNOWN = LIBSWO_PACKET_TYPE_UNKNOWN,
	SYNC = LIBSWO_PACKET_TYPE_SYNC,
	OF = LIBSWO_PACKET_TYPE_OF,
	LTS = LIBSWO_PACKET_TYPE_LTS,
	GTS1 = LIBSWO_PACKET_TYPE_GTS1,
	GTS2 = LIBSWO_PACKET_TYPE_GTS2,
	EXT = LIBSWO_PACKET_TYPE_EXT,
	INST = LIBSWO_PACKET_TYPE_INST,
	HW = LIBSWO_PACKET_TYPE_HW,
	DWT_EVTCNT = LIBSWO_PACKET_TYPE_DWT_EVTCNT,
	DWT_EXCTRC = LIBSWO_PACKET_TYPE_DWT_EXCTRC,
	DWT_PC_SAMPLE = LIBSWO_PACKET_TYPE_DWT_PC_SAMPLE,
	DWT_PC_VALUE = LIBSWO_PACKET_TYPE_DWT_PC_VALUE,
	DWT_ADDR_OFFSET = LIBSWO_PACKET_TYPE_DWT_ADDR_OFFSET,
	DWT_DATA_VALUE = LIBSWO_PACKET_TYPE_DWT_DATA_VALUE
};

enum class LocalTimestampRelation {
	SYNC = LIBSWO_LTS_REL_SYNC,
	TS = LIBSWO_LTS_REL_TS,
	SRC = LIBSWO_LTS_REL_SRC,
	BOTH = LIBSWO_LTS_REL_BOTH
};

enum class ExtensionSource {
	ITM = LIBSWO_EXT_SRC_ITM,
	HW = LIBSWO_EXT_SRC_HW
};

enum class ExceptionTraceFunction {
	RESERVED = LIBSWO_EXCTRC_FUNC_RESERVED,
	ENTER = LIBSWO_EXCTRC_FUNC_ENTER,
	EXIT = LIBSWO_EXCTRC_FUNC_EXIT,
	RETURN = LIBSWO_EXCTRC_FUNC_RETURN
};

enum class DecoderFlags {
	NONE = 0,
	EOS = LIBSWO_DF_EOS
};

const char LOG_DOMAIN_DEFAULT[] = LIBSWO_LOG_DOMAIN_DEFAULT;
const unsigned int LOG_DOMAIN_MAX_LENGTH = LIBSWO_LOG_DOMAIN_MAX_LENGTH;

typedef int (*LogCallback)(LogLevel level, const std::string &message,
		void *user_data);

class LIBSWO_API Error : public std::exception
{
public:
	Error(int error_code);
	~Error(void) throw();

	const char *what(void) const throw();
	const int code;
};

class LIBSWO_API Packet
{
public:
	virtual ~Packet(void) = 0;

	PacketType get_type(void) const;
	size_t get_size(void) const;
	virtual const std::string to_string(void) const = 0;
protected:
	union libswo_packet _packet;
};

typedef int (*DecoderCallback)(const Packet &packet, void *user_data);

class LIBSWO_API PayloadPacket : public Packet
{
public:
	virtual ~PayloadPacket(void) = 0;

	const std::vector<uint8_t> get_data(void) const;

	static const unsigned int MAX_SIZE = LIBSWO_MAX_PAYLOAD_SIZE;
};

class LIBSWO_API Unknown : public PayloadPacket
{
public:
	Unknown(const struct libswo_packet_unknown *packet);
	Unknown(const union libswo_packet *packet);

	const std::string to_string(void) const;
};

class LIBSWO_API Synchronization : public Packet
{
public:
	Synchronization(const struct libswo_packet_sync *packet);
	Synchronization(const union libswo_packet *packet);

	const std::string to_string(void) const;
};

class LIBSWO_API Overflow : public PayloadPacket
{
public:
	Overflow(const struct libswo_packet_of *packet);
	Overflow(const union libswo_packet *packet);

	const std::string to_string(void) const;
};

class LIBSWO_API LocalTimestamp : public PayloadPacket
{
public:
	LocalTimestamp(const struct libswo_packet_lts *packet);
	LocalTimestamp(const union libswo_packet *packet);

	LocalTimestampRelation get_relation() const;
	uint32_t get_value(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API GlobalTimestamp1 : public PayloadPacket
{
public:
	GlobalTimestamp1(const struct libswo_packet_gts1 *packet);
	GlobalTimestamp1(const union libswo_packet *packet);

	uint32_t get_value(void) const;
	bool get_clkch(void) const;
	bool get_wrap(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API GlobalTimestamp2 : public PayloadPacket
{
public:
	GlobalTimestamp2(const struct libswo_packet_gts2 *packet);
	GlobalTimestamp2(const union libswo_packet *packet);

	uint32_t get_value(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API Extension : public PayloadPacket
{
public:
	Extension(const struct libswo_packet_ext *packet);
	Extension(const union libswo_packet *packet);

	ExtensionSource get_source(void) const;
	uint32_t get_value(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API SourcePacket : public PayloadPacket
{
public:
	virtual ~SourcePacket(void);

	uint8_t get_address(void) const;
	const std::vector<uint8_t> get_payload(void) const;
	uint32_t get_value(void) const;

	virtual const std::string to_string(void) const = 0;

	static const unsigned int MAX_ADDRESS = LIBSWO_MAX_SOURCE_ADDRESS;
};

class LIBSWO_API Instrumentation : public SourcePacket
{
public:
	Instrumentation(const struct libswo_packet_inst *packet);
	Instrumentation(const union libswo_packet *packet);

	const std::string to_string(void) const;
};

class LIBSWO_API Hardware : public SourcePacket
{
public:
	Hardware(const struct libswo_packet_hw *packet);
	Hardware(const union libswo_packet *packet);

	const std::string to_string(void) const;
};

class LIBSWO_API EventCounter : public Hardware
{
public:
	EventCounter(const struct libswo_packet_dwt_evtcnt *packet);
	EventCounter(const union libswo_packet *packet);

	bool get_cpi(void) const;
	bool get_exc(void) const;
	bool get_sleep(void) const;
	bool get_lsu(void) const;
	bool get_fold(void) const;
	bool get_cyc(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API ExceptionTrace : public Hardware
{
public:
	ExceptionTrace(const struct libswo_packet_dwt_exctrc *packet);
	ExceptionTrace(const union libswo_packet *packet);

	uint16_t get_exception(void) const;
	ExceptionTraceFunction get_function(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API PCSample : public Hardware
{
public:
	PCSample(const struct libswo_packet_dwt_pc_sample *packet);
	PCSample(const union libswo_packet *packet);

	bool get_sleep(void) const;
	uint32_t get_pc(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API PCValue : public Hardware
{
public:
	PCValue(const struct libswo_packet_dwt_pc_value *packet);
	PCValue(const union libswo_packet *packet);

	uint8_t get_comparator(void) const;
	uint32_t get_pc(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API AddressOffset : public Hardware
{
public:
	AddressOffset(const struct libswo_packet_dwt_addr_offset *packet);
	AddressOffset(const union libswo_packet *packet);

	uint8_t get_comparator(void) const;
	uint16_t get_offset(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_API DataValue : public Hardware
{
public:
	DataValue(const struct libswo_packet_dwt_data_value *packet);
	DataValue(const union libswo_packet *packet);

	bool get_wnr(void) const;
	uint8_t get_comparator(void) const;
	uint32_t get_data_value(void) const;

	const std::string to_string(void) const;
};

class LIBSWO_PRIV DecoderCallbackHelper
{
public:
	DecoderCallback callback;
	void *user_data;
};

class LIBSWO_PRIV LogCallbackHelper
{
public:
	LogCallback callback;
	void *user_data;
};

class LIBSWO_API Context
{
public:
	Context(size_t buffer_size = DEFAULT_BUFFER_SIZE);
	Context(uint8_t *buffer, size_t buffer_size = DEFAULT_BUFFER_SIZE);
	~Context(void);

	LogLevel get_log_level(void) const;
	void set_log_level(LogLevel level);

	std::string get_log_domain(void) const;
	void set_log_domain(const std::string &domain);
	void set_log_callback(LogCallback callback, void *user_data = NULL);

	void set_callback(DecoderCallback callback, void *user_data = NULL);

	void feed(const uint8_t *data, size_t length);
	void decode(DecoderFlags flags = DecoderFlags::NONE);

	static const size_t DEFAULT_BUFFER_SIZE = LIBSWO_DEFAULT_BUFFER_SIZE;
private:
	struct libswo_context *_context;
	DecoderCallbackHelper _decoder_callback;
	LogCallbackHelper _log_callback;
};

class LIBSWO_API Version
{
public:
	static int get_package_major(void);
	static int get_package_minor(void);
	static int get_package_micro(void);
	static std::string get_package_string(void);

	static int get_library_current(void);
	static int get_library_revision(void);
	static int get_library_age(void);
	static std::string get_library_string(void);
};

inline DecoderFlags operator|(DecoderFlags lhs, DecoderFlags rhs)
{
	using type = std::underlying_type<DecoderFlags>::type;

	return static_cast<DecoderFlags>(static_cast<type>(lhs) |
		static_cast<type>(rhs));
}

}

#endif /* LIBSWOCXX_HPP */
